#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

int main(){
	int n = 8, i=0, j=0, count=0, threads =omp_get_num_procs();
	FILE *f;
        char c, str[20], file_name[20];
	char ** words;

	words = (char **) malloc(n*sizeof(char *));
        words[j] = (char *) malloc(100*sizeof(char));

	scanf("%s\n%s", &file_name, &str);
	
	//Open the file
	f = fopen(file_name, "r");
	if(f == NULL){
		printf("Cannot open file.\n");
		exit(0);
	}
	c = getc(f);

	//Create linked list
	while(c != EOF){
		if(c == '\0' || c == '\b' || c == ' ' || c == '\t' || c == '\n' ){
			if(j >= n){
				n *= 2;
				words = (char **) realloc(words, n*sizeof(char *));
			}
				
			for(i=i; i<15; i++)
				words[j][i] = '\0';
	
			i=0;
			j++;
			words[j] = (char *) malloc(100*sizeof(char));
		}else{
			words[j][i] = c;
			i++;
		}

		c = getc(f);
	}

	double t_start = omp_get_wtime();
  

#pragma omp taskloop shared(str,words) num_tasks(threads)
	for(i=0; i<j; i++){
		if(strcmp(str, words[i]) == 0){
		#pragma omp atomic
			count += 1;
		}

	} 

	double t_end = omp_get_wtime();
	
	printf("%s: %d occurrences of %d\n", str, count, j);
	printf("Time: %lf\n", t_end - t_start);   
  	
  	fclose(f);
}


 
